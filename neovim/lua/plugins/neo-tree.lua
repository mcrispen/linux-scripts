return {
  "nvim-neo-tree/neo-tree.nvim",
  branch = "v3.x",
  dependencies = {
    "nvim-lua/plenary.nvim",
    "nvim-tree/nvim-web-devicons", 
    "MunifTanjim/nui.nvim",
  },
  config = function () 
    local builtin = require("telescope.builtin")
    vim.keymap.set("n", "<leader>dt", ":Neotree filesystem reveal left<CR>")
    vim.keymap.set("n", "l", builtin.open)
  end
}
