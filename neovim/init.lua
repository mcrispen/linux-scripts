-- Generic VIM Settings --
vim.cmd("set expandtab")
vim.cmd("set tabstop=2")
vim.cmd("set softtabstop=2")
vim.cmd("set shiftwidth=2")
vim.g.mapleader = " "

-- Install Lazy Package Manager --
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable",
    lazypath
  })
end
vim.opt.rtp:prepend(lazypath)

-- Install Lazy Packages --
local opts = {}

require("lazy").setup("plugins")


-- Setup color scheme --
require("catppuccin").setup()
vim.cmd.colorscheme "catppuccin"

-- Setup Telescope --
local builtin = require("telescope.builtin")
vim.keymap.set("n", "<leader>..", builtin.find_files, {})
vim.keymap.set("n", "<leader>.g", builtin.live_grep, {})

-- Setup Treesitter --
local config = require("nvim-treesitter.configs")
config.setup({
  ensure_installed = {"lua", "javascript", "python"},
  highlight = {enabled = true},
  indent = {enabled = true}
})

-- Setup NeoTree --
vim.keymap.set("n", "<leader>dt", ":Neotree filesystem reveal left<CR>")

