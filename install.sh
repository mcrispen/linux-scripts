#!/bin/bash
set -e

efivars=$(ls /sys/firmware/efi/efivars)

if [ -z "$efivars" ]
then
		echo 'Not booted in EFI mode. Exiting...'
		exit
fi

ping_result=$(ping -c 1 archlinux.org)

if [[ $ping_result == *"0 received"* ]]
then
		echo 'No internet connection. Exiting...'
		exit
else
		echo 'Networking verified'
fi



disks=$(lsblk | grep 'disk' | cut -d' ' -f1)
num_disks=$(lsblk | grep 'disk' | cut -d' ' -f1 | wc -l)
if [ $num_disks -eq 1 ]
then DISK="/dev/$disks"
else
		select disk in $disks
		do
				DISK="/dev/$disk"
				break
		done
fi

echo "DISK is $DISK"

parted --script $DISK \
			 mklabel gpt \
			 mkpart gpt fat32 17.4kiB 500MiB \
			 set 1 esp on \
			 mkpart ext4 500MiB 1500MiB \
			 mkpart ext4 1500MiB 100%

	DISK_BOOT=$DISK"1"
	DISK_SWAP=$DISK"2"
	DISK_ROOT=$DISK"3"

	mkswap $DISK_SWAP

	mkfs.ext4 $DISK_ROOT
	mkfs.fat -F 32 $DISK_BOOT

	mkdir /mnt/arch

	mount $DISK_ROOT /mnt/arch

	mkdir /mnt/arch/boot
	mount $DISK_BOOT /mnt/arch/boot

	swapon $DISK_SWAP

read -p 'Hostname: ' hostname < /dev/tty
echo

read -p 'Root Password: ' -s password < /dev/tty
echo

	reflector --country US --protocol https > /etc/pacman.d/mirrorlist

	pacstrap /mnt/arch base linux linux-firmware

pacstrap /mnt/arch grub efibootmgr

pacstrap /mnt/arch intel-ucode

	pacstrap /mnt/arch networkmanager

	pacstrap /mnt/arch \
		vim \
		sudo \
		git \
		stow \
		base-devel \
		rofi \
		dmenu \
		python-pip \
		pulseaudio \
		pavucontrol \
		alsa-utils \
		arandr \
		nitrogen \
		gcc \
		openssh \
		libvterm \
		cmake \
		fish \
		inetutils \
		tmux \
		man-db \
		man-pages \
		xorg-server \
		xorg-xinit \
		xorg-xrdb \
		sddm \
		qtile \
		mypy \
		alacritty \
		ttf-dejavu \
		emacs \
		hicolor-icon-theme \
		adwaita-icon-theme \
		lxappearance \
		xdg-user-dirs \
		exa \
		archlinux-wallpaper \
		lxappearance \
		picom

	genfstab -U /mnt/arch >> /mnt/arch/etc/fstab

arch-chroot /mnt/arch /bin/bash <<EOF
	set -e
	ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
	hwclock --systohc
	sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen
	locale-gen
	echo "$hostname" > /etc/hostname
	echo "root:$password" | chpasswd
	grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
	grub-mkconfig -o /boot/grub/grub.cfg
	ln -s '/lib/systemd/system/sddm.service' '/etc/systemd/system/multi-user.target.wants/sddm.service'
	useradd mcrispen --create-home -s /usr/bin/fish
	groupadd sudo
	usermod -aG sudo mcrispen
	echo "mcrispen:$password" | chpasswd
	echo '%sudo ALL=(ALL:ALL) NOPASSWD: ALL' >> /etc/sudoers
	cd /home/mcrispen
	sudo -u mcrispen xdg-user-dirs-update
	sudo -u mcrispen git clone https://gitlab.com/mcrispen/linux-scripts.git
	cd linux-scripts/
	sudo -u mcrispen stow --adopt dotfiles
	cd /home/mcrispen
	sudo -u mcrispen git clone https://aur.archlinux.org/yay.git
	sudo -u mcrispen env --chdir=/home/mcrispen/yay makepkg -si --noconfirm
	yes | sudo -u mcrispen yay -S qtile_extras
	curl https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install | sudo -u mcrispen fish
	sudo -u mcrispen fish -c 'omf install bira'
	yes | sudo -u mcrispen yay -S sweet-theme-full-git nerd-fonts-sf-mono
	sudo -u mcrispen mkdir /home/mcrispen/.local/share/fonts
	sudo -u mcrispen curl https://gitlab.com/mcrispen/linux-scripts/-/raw/master/.fonts/ConkySymbols.ttf -o /home/mcrispen/.local/share/fonts/ConkySymbols.ttf
	sudo -u mcrispen fc-cache -f -v
	sudo -u mcrispen mkdir /home/mcrispen/roam-notes /home/mcrispen/org-agenda /home/mcrispen/org-roam
	sudo -u mcrispen /usr/bin/emacs -l /home/mcrispen/.emacs.d/init.el
  git clone https://github.com/ritave/xeventbind.git
  cd xeventbind
  make
  cp xeventbind /bin/
EOF

umount -R /mnt/arch
reboot
