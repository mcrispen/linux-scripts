[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias ll='ls -lah'
alias l='ls -lh'
alias lr='ls -aR'
alias ld='du -sh'

PS1='[\u@\h \W]\$ '

export EDITOR=vim
export VISUAL=vim
