import os
import json
import re
import socket
import subprocess
from typing import List  # noqa: F401
from libqtile import layout, bar, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen, Rule, KeyChord
from libqtile.lazy import lazy
from libqtile.widget import Spacer
from qtile_extras import widget
from qtile_extras.widget.decorations import RectDecoration, BorderDecoration
from copy import deepcopy

colors = {
    "yellow": [["#FFBE0B", "#FFBE0B"]],
    "orange": [["#FB5607", "#FB5607"]],
    "red": [["#e60e38", "#e60e38"]],
    "purple": [
        ["#b263d6", "#b263d6"],
        ["#380061", "#380061"]
    ],
    "blue": [["#0095f2", "#0095f2"], ["#2d395e", "#2d395e"]],
    "green": [["#00a336", "#00a336"]],
    "primary": ["#282C34", "#282C34"],
    "secondary": ["#305275", "#305275"],
		"grey": [["#DDDDDD"],["#DDDDDD"]]
}

mod = "mod4"
mod1 = "alt"
mod2 = "control"
home = os.path.expanduser('~')
keys = []
dgroups_key_binder = None
dgroups_app_rules = []
user = os.getlogin()
main = None

interface = 'ens33'
try:
    with open(os.path.expanduser('~') + '/.interface') as f:
        interface = f.read()
except Exception:
    print(f'~/.interface is missing on this system.')

keys.extend([
    Key([mod], "q", lazy.window.kill()),
    Key([mod, "shift"], "r", lazy.reload_config()),
])

# MOUSE CONFIGURATION
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size())
]

keys.extend([
    Key([mod], "f", lazy.window.toggle_fullscreen()),
    # RESIZE UP, DOWN, LEFT, RIGHT
    Key([mod, "shift"], "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "shift"], "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),
])

keys.extend([
    # QTILE LAYOUT KEYS
    Key([mod], "space", lazy.next_layout()),

    # FLIP LAYOUT FOR MONADTALL/MONADWIDE
    Key([mod, "shift"], "f", lazy.layout.flip()),

    # MOVE WINDOWS UP OR DOWN BSP LAYOUT
    Key([mod, "shift", "control"], "k", lazy.layout.shuffle_up()),
    Key([mod, "shift", "control"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift", "control"], "h", lazy.layout.shuffle_left()),
    Key([mod, "shift", "control"], "l", lazy.layout.shuffle_right()),

    # TOGGLE FLOATING LAYOUT
    Key([mod, "shift"], "space", lazy.window.toggle_floating()),
])

keys.extend([
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),
])

keys.extend([
    # Run Custom Scripts
    Key([mod, 'shift'], 'c', lazy.spawn(f'/home/{user}/.scripts/open_project')),
    Key([mod, 'shift'], 'w', lazy.spawn(f'/home/{user}/.screenlayout/home.sh')),
    Key([mod, 'shift'], 's', lazy.spawn(f'/home/{user}/.scripts/run_script.sh')),

    # Open Programs
    Key([mod, "shift"], 'n', lazy.spawn('notepadqq')),
    Key([mod], 'c', lazy.spawn('kcalc')),
    Key([mod], "Return", lazy.spawn("alacritty")),
    Key([mod, "shift"], "Return", lazy.spawn("pcmanfm")),
    Key([mod, "shift"], "d", lazy.spawn("rofi -show drun")),
    Key([mod, "shift"], "c", lazy.spawn(f"bash /home/{user}/.scripts/open_project")),
    Key([mod], "b", lazy.spawn("/usr/bin/xdg-open https://google.com")),
    Key([mod], 'z', lazy.spawn('obs')),
    Key([mod], 'g', lazy.spawn(f'/home/{user}/ds360/ds360')),
    Key([mod], 's', lazy.spawn('flameshot gui -c')),
    Key([mod], 'v', lazy.spawn('/usr/bin/bash -c "QT_STYLE_OVERRIDE=kvantum-dark virtualbox"')),
    Key([mod], 'x', lazy.spawn('obs-cli --host server01.redsky.local --port 4444 --password nopassword replaybuffer save')),
    Key([mod], 'p', lazy.spawn(f'/home/{user}/.scripts/type-clipboard.sh')),
		Key([mod, 'shift'], 'l', lazy.spawn('flatpak run com.logseq.Logseq')),
		Key([mod], 'v', lazy.spawn('roficlip')),
		Key([mod], 'n', lazy.spawn('google-chrome-stable --user-data-dir=.config/google-chrome-for-notion/ --app=https://notion.so')),
		Key([mod], 'p', lazy.spawn('google-chrome-stable --user-data-dir=.config/google-chrome-postman/ --app=https://secdef.postman.co/home')),
		Key([mod], 't', lazy.spawn(f'bash /home/{user}/.scripts/run_teams')),
		Key([mod, "shift"], 'k', lazy.spawn(f'bash /home/{user}/.scripts/toggle_conky')),

    # Key Chords

    ## Emacs
    KeyChord([mod], 'e', [
        Key([], 'e', lazy.spawn('emacsclient --create-frame --alternate-editor="emacs"')),
        Key([], 'h', lazy.spawn('emacsclient --create-frame --alternate-editor="emacs" ~/')),
        Key([], 'l', lazy.spawn('emacsclient --create-frame --alternate-editor="emacs"  ~/linux-scripts')),
        Key([], 'q', lazy.spawn('emacsclient --create-frame --alternate-editor="emacs" ~/linux-scripts/qtile/.config/qtile/config.org')),
        Key([], 'p', lazy.spawn('emacsclient --create-frame --alternate-editor="emacs"  ~/projects')),
        Key([], 's', lazy.spawn('emacsclient --create-frame --alternate-editor="emacs"  --execute "(doom/switch-to-scratch-buffer)"')),
        Key([], 'r', lazy.spawn(f'/home/{user}/.scripts/emacs-ssh.sh')),
    ])
])

groups = []
group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0",]
group_labels = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0",]
group_layouts = ["monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall",]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

for i in groups:
    keys.extend([
        Key([mod], i.name, lazy.group[i.name].toscreen()),
        Key([mod], "Tab", lazy.screen.next_group()),
        Key([mod, "shift" ], "Tab", lazy.screen.prev_group()),
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
        Key([mod, "control"], i.name, lazy.window.togroup(i.name) , lazy.group[i.name].toscreen()),
    ])

layout_theme = {
    "border_width":1,
    "border_focus": colors['blue'][1],
    "border_normal": colors['secondary']
}

layouts = [
    layout.MonadTall(margin=8, **layout_theme),
    layout.MonadWide(margin=8, **layout_theme),
    layout.Floating(**layout_theme),
]

rect_decoration = RectDecoration(
                      radius=18,
                      filled=True,
                      group=False,
                      padding=-2,
                      use_widget_background=False,
                      colour=colors['purple'][1]
                  )

widget_defaults = {
	'font': 'DejaVu Bold',
	'fontsize': 12,
	'foreground': ["#FFF", "#FFF"],
	'background': colors['primary'],
}

def get_widgets():
    prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())
    widgets_list = [

        widget.Net(format='{down} \uf175 \uf176 {up}', interface=interface),

        widget.GroupBox(
            margin_y = 3,
            borderwidth = 2,
            disable_drag = True,
            active = colors["blue"][0],
            inactive = colors["secondary"],
            highlight_method = "line",
            highlight_color = [colors["primary"][0], "#5f5766"],
            this_current_screen_border = colors["blue"][0],
            this_screen_border = colors["blue"][0],
            other_current_screen_border = colors["blue"][0],
            other_screen_border = colors["blue"][0],
        ),

        widget.Spacer(decorations=[]),

        widget.CurrentLayout(
            foreground=colors['purple'][0],
            decorations=[BorderDecoration(border_width=[0, 0, 2, 0],
                                          colour=colors['purple'][0],
                                          padding_x=5)],
        ),

        widget.Spacer(decorations=[], length=5),

        widget.Net(
            format='{down} \uf175 \uf176 {up}',
            interface='enp0s31f6',
            foreground=colors['grey'][0],
            decorations=[BorderDecoration(border_width=[0, 0, 2, 0],
                                          colour=colors['grey'][0],
                                          padding_x=5)],
        ),

        widget.Spacer(decorations=[], length=5),

        widget.Memory(
            format='[M] {MemPercent: 2.2f}%',
            padding=None,
            padding_x=-5,
            foreground=colors['red'][0],
            decorations=[BorderDecoration(border_width=[0, 0, 2, 0],
                                          colour=colors['red'][0],
                                          padding_x=5)],
        ),

        widget.Spacer(decorations=[], length=5),

        widget.CPU(
            format='[C] {load_percent:3.2f}%',
            foreground=colors['green'][0],
            decorations=[BorderDecoration(border_width=[0, 0, 2, 0],
                                          colour=colors['green'][0],
                                          padding_x=5)],
        ),

        widget.Spacer(decorations=[], length=5),

        widget.Clock(
            format='%Y-%m-%d %H:%M:%S',
            margin_x=2,
            foreground=colors['yellow'][0],
            decorations=[BorderDecoration(border_width=[0, 0, 2, 0],
                                          colour=colors['yellow'][0],
                                          padding_x=5)],
        ),

    ]
    return widgets_list

bar_config = {
    'size': 24,
    'opacity': 0.8,
    'background': '#00000000'
}
screens = [
    Screen(top=bar.Bar(widgets=get_widgets(), **bar_config)),
    Screen(top=bar.Bar(widgets=get_widgets(), **bar_config)),
    Screen(top=bar.Bar(widgets=get_widgets(), **bar_config))
]

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    try:
        subprocess.call(['bash', home + '/.config/qtile/scripts/autostart.sh'])
    except Exception as err:
        print(f'Failed to run autostart.sh {err}')

@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])

@hook.subscribe.client_new
def set_floating(window):
    if (window.window.get_wm_transient_for()
            or window.window.get_wm_type() in floating_types):
        window.floating = True

floating_types = ["notification", "toolbar", "splash", "dialog"]


follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(wm_class='Arcolinux-welcome-app.py'),
    Match(wm_class='Arcolinux-tweak-tool.py'),
    Match(wm_class='Arcolinux-calamares-tool.py'),
    Match(wm_class='confirm'),
    Match(wm_class='dialog'),
    Match(wm_class='download'),
    Match(wm_class='error'),
    Match(wm_class='file_progress'),
    Match(wm_class='notification'),
    Match(wm_class='splash'),
    Match(wm_class='toolbar'),
    Match(wm_class='Arandr'),
    Match(wm_class='feh'),
    Match(wm_class='Galculator'),
    Match(wm_class='arcolinux-logout'),
    Match(wm_class='xfce4-terminal'),

],  fullscreen_border_width = 0, border_width = 0)
auto_fullscreen = True

focus_on_window_activation = "focus" # or smart

wmname = "LG3D"
