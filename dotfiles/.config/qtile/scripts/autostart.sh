#!/bin/bash

	picom -b --experimental-backends || picom -b /usr/bin/emacs --daemon
	clipster -d &
	conky &
	xeventbind resolution ~/.scripts/on_resolution_change.sh

	nitrogen --restore

	setxkbmap -option ctrl:nocaps

	if [ -d ~/.screenlayout ]
		then
			for i in $(ls -A ~/.screenlayout)
				do /usr/bin/bash "$i"
			done
	fi
