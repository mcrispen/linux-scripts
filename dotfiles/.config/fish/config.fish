set EDITOR 'fish -c /usr/bin/emacs --create-frame --alternate-editor="emacs"'
set VISUAL 'fish -c /usr/bin/emacs --create-frame --alternate-editor="emacs"'
set BROWSER "brave"

set PATH "$HOME/.local/bin" $PATH

  set -gx HOST '/mnt/hgfs/host'
  set -gx HOST_DOCUMENTS "/mnt/hgfs/host/OneDrive - Secure Cyber Defense/Documents"
  set -gx HOST_DOWNLOADS "/mnt/hgfs/host/Downloads"

  alias ls="exa -a --icons --sort type"
  alias ll="exa -alh --icons --sort type --git"
  alias l="exa -lh --icons --sort type --git"
	alias lt="exa -alh --icons --sort type --git -T"
	alias lls="exa -alh --icons --sort size --reverse --git"
  alias tree="tree -c"

  alias eip="curl ipinfo.io/ip"
  alias iip="ip addr | grep inet | grep '/24' | cut -d' ' -f6"
  alias ssh="env TERM=xterm ssh"

  alias emacs='fish -c /usr/bin/emacs --create-frame --alternate-editor="emacs"'

  alias please='sudo "$BASH" -c "$(history -p !!)"'
  alias gh="history | grep"
  alias tldr="python3 -m tldr"
  alias fresource=". ~/.config/fish/config.fish"
  alias bresource=". ~/.bashrc"
  alias real=". ~/.bash_aliases"

  alias drun="docker run -it --rm"

  alias pacman="sudo pacman"
  alias install="sudo pacman -S"
	alias upgrade="sudo pacman -Syu"

	alias virtualbox="QT_STYLE_OVERRIDE=kvantum-dark virtualbox"

	export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring

function replace
  sed "s/$argv[1]/$argv[2]/g"
end

function split
  awk -F"$argv[1]" "{print \$$argv[2]}"
end

function trash
  set filename $(echo "$argv[1]" | replace '\/' '_')
  echo $filename
  tar cvzf ~/.trash/$filename.tar $argv[1]
  rm -rf $argv[1]
end

function ld
  if test -n "$argv[1]"
      du -sh $argv[1]/*
  else
      du -sh ./*
  end
end
