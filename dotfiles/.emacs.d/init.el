	(setq inhibit-startup-message t)
	(scroll-bar-mode -1)
	(tool-bar-mode -1)
	(tooltip-mode -1)
	(set-fringe-mode 10)
	(menu-bar-mode -1)

	;; Set font
	(set-face-attribute 'default nil :font "DejaVu Sans Mono" :height 125)

	(setq-default tab-width 2)
	(setq left-fringe-width 20)

	(require 'package)
	(setq package-archives '(("melpa" . "https://melpa.org/packages/")
													 ("melpa-stable" . "https://stable.melpa.org/packages/")
													 ("org" . "https://orgmode.org/elpa/")
													 ("elpa" . "https://elpa.gnu.org/packages/")))
	(package-initialize)
	(unless package-archive-contents 
		(package-refresh-contents))
	(unless (package-installed-p 'use-package)
		(package-install 'use-package))
	(require 'use-package)
	(setq use-package-always-ensure t)

	(defvar bootstrap-version)
	(let ((bootstrap-file
				 (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
				(bootstrap-version 5))
		(unless (file-exists-p bootstrap-file)
			(with-current-buffer
					(url-retrieve-synchronously
					 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
					 'silent 'inhibit-cookies)
				(goto-char (point-max))
				(eval-print-last-sexp)))
		(load bootstrap-file nil 'nomessage))

	(use-package all-the-icons
		:if (display-graphic-p))

	(setq backup-directory-alist `(("." . "~/.saves")))

(use-package doom-themes
	:ensure t
	:config
	;; Global settings (defaults)
	(setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
				doom-themes-enable-italic t) ; if nil, italics is universally disabled
	(load-theme 'doom-one t)
	;; Enable flashing mode-line on errors
	(doom-themes-visual-bell-config)
	;; Enable custom neotree theme (all-the-icons must be installed!)
	(doom-themes-neotree-config)
	;; or for treemacs users
	(setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
	(doom-themes-treemacs-config)
	;; Corrects (and improves) org-mode's native fontification.
	(doom-themes-org-config))

(set-frame-parameter (selected-frame) 'alpha '(85 . 50))
(add-to-list 'default-frame-alist '(alpha . (85 . 50)))

(defun toggle-transparency ()
  (interactive)
  (let ((alpha (frame-parameter nil 'alpha)))
    (set-frame-parameter
     nil 'alpha
     (if (eql (cond ((numberp alpha) alpha)
                    ((numberp (cdr alpha)) (cdr alpha))
                    ;; Also handle undocumented (<active> <inactive>) form.
                    ((numberp (cadr alpha)) (cadr alpha)))
              100)
         '(85 . 50) '(100 . 100)))))

	(use-package undo-tree
		:config
		(global-undo-tree-mode)
		(setq undo-tree-history-directory-alist '(("." . "~/.emacs.d/undo"))))

	(use-package evil
		:ensure t
		:init
		(setq evil-want-integration t) ;; This is optional since it's already set to t by default.
		(setq evil-want-keybinding nil)
		:config
		(evil-mode 1))

	(use-package evil-collection
		:after evil
		:ensure t
		:config
		(evil-collection-init))
	(setq evil-collection-setup-minibuffer t)
  (fset 'evil-visual-update-x-selection 'ignore)

	(use-package which-key
		:init
		(which-key-mode)
		:config
		(require 'which-key)
		(which-key-setup-minibuffer)
		(setq which-key-idle-delay 0.3))

	(use-package general
		:config
		(require 'general))
	(general-create-definer leader-key
		:prefix "SPC")
	;; General Keybindings

	(use-package doom-modeline
		:ensure t
		:init (doom-modeline-mode 1)
		:custom ((doom-modeline-height 15)))

	(use-package ivy
		:diminish
		:bind (:map ivy-minibuffer-map
								("TAB" . ivy-alt-done)
								("C-l" . ivy-alt-done)
								("C-S-l" . ivy-immediate-done)
								("C-j" . ivy-next-line)
								("C-k" . ivy-previous-line)
								:map ivy-switch-buffer-map
								("C-k" . ivy-previous-line)
								("C-l" . ivy-done)
								("C-d" . ivy-switch-buffer-kill)
								:map ivy-reverse-i-search-map
								("C-k" . ivy-previous-line)
								("C-d" . ivy-reverse-i-search-kill))
		:config)
	(ivy-mode 1)

	(use-package ivy-rich
		:config
		(ivy-rich-mode 1))

	(use-package tabspaces
		:hook (after-init . tabspaces-mode)
		:commands (tabspaces-create-workspace
							 tabspaces-create-new-project-and-workspace
							 tabspaces-open-existing-project-and-workspace
							 tabspaces-switch-workspace)
		:custom
		(tabspaces-use-filtered-buffers-as-default t)
		(tabspaces-default-tab "Default")
		(tabspaces-remove-to-default t)
		(tabspaces-include-buffers '("*scratch*"))
		(tab-bar-show nil))

	(require 'recentf)
	(recentf-mode 1)

(column-number-mode)
(global-display-line-numbers-mode t)

(dolist (mode '(org-mode-hook
								term-mode-hook
								eshell-mode-hook))
	(add-hook mode (lambda () (display-line-numbers-mode 0))))

(use-package rainbow-delimiters
	:hook (prog-mode . rainbow-delimiters-mode))

(use-package vterm
 	:ensure t)

(use-package projectile
	:diminish projectile-mode
	:config (projectile-mode)
	:bind-keymap
	("C-c p" . projectile-command-map)
	:init
	(when (file-directory-p "~/Projects/Code")
		(setq projectile-project-search-path '("~/Projects/Code")))
	(setq projectile-switch-project-action #'projectile-dired))

;; Counsel Integration
(use-package counsel-projectile
	:after projectile
	:config
	(counsel-projectile-mode 1))

(use-package magit
	:commands (magit-status magit-get-current-branch)
	:custom
	(magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))
(use-package forge)

						 ;;; json-to-org-table.el --- Converts json string to linked org table -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2020 Joshua Person
;;
;; Author: Joshua Person <http://github.com/noonker>
;; Maintainer: Joshua Person <ceo@legitimate.company>
;; Created: December 06, 2020
;; Modified: December 06, 2020
;; Version: 0.0.1
;; Keywords:
;; Homepage: https://github.com/noonker/json-to-org-table
;; Package-Requires: ((emacs 27.1))
;;
;; This file is not part of GNU Emacs.
;;
						;;; Commentary:
;;
;;  Converts json string to linked org table
;;
						;;; Code:
						;;; TODO: Better Examples

(defvar j2t-debug nil)

(defvar j2t-cs-map [("\r" "")
										("\n" "")]
	"Map of characters to replace in json string.")

(defun j2t-cs (str)
	"Clean String.
						Replace any string org mode wouldn't like according to the j2t-cs-map
						STR: String to be replaced
						RETURNS: A string with problematic characters returned"
	(seq-reduce
	 (lambda (s m) (replace-regexp-in-string (car m) (cadr m) s))
	 j2t-cs-map (format "%s" str)))

(defun j2t-lf (key &optional ref i)
	"Convert to link Link Format based on available args.
						KEY: String or Symbol that becomes the name of the table
						REF: If there is a Reference that becomes a subkey of the link
						I: Is the Index for links in vectors"
	(cond (i (format "[[%s_%s%s]]" key ref (number-to-string i)))
				(ref (format "[[%s_%s]]" key ref))
				(t (format "[[%s]]" key))))

(defun j2t-hp (key value)
	"Hashmap Print prints a hashmap key-value table row.
						KEY: Hashmap key column
						VALUE: Hashmap value column"
	(format "|%s|%s|\n" (j2t-cs key) (j2t-cs value)))

(defmacro j2t-c+ (&rest str)
	"HACK: Concatenates all args and update the value of cur with new STR.
						There's probably a better way to do this but this keeps things as clean
						as possible in the =tablify= function."
	`(setq cur (concat cur (concat ,@str ))))

(defun j2t-parse-vector-vector (elt)
	"The row parser for a vector of vectors.
						ELT: Is a vector to be turned into a table row
						RETURNS: A table row representing the values of a vector"
	(let ((cur ""))
		(j2t-c+ "|")
		(mapc (lambda (x) (j2t-c+ (j2t-cs (format "%s" x)) "|" )) elt)
		(j2t-c+ "\n")
		cur
		)
	)

(defun j2t-parse-hashmap-vector (elt &optional ref i)
	"A row parser for a vector element composed of hashmaps.
						ELT: A dotted pair cons representing a json hashmap
						REF: Reference if this is a linked table
						I: Optional Index for multiple linked tables
						RETURNS: The table row representing values of a hashmap and a
										 list of subtables to create if applicable
						EXAMPLE: ((a . (b . 2)) (c . d) (e . f)) -> '(\"|[[a]]|d|f|]\" '(a (b .2) 1))"
	(let ((cur "")
				(keys (mapcar #'car elt))
				(nex '()))
		(mapcar (lambda (key)
							(let ((value (alist-get key elt)))
								(if (consp value)
										(progn
											(j2t-c+ (j2t-lf key ref i) "|")
											(setq nex (append nex '('(key value i)))))
									(j2t-c+ (j2t-cs value) "|" )))
							) keys)
		`(,cur ,nex)
		))


(defun j2t-parse-hash-element (elt &optional ref)
	"A row parser for elements of a hash map.
						ELT: A dotted pair cons representing a json hashmap
						REF: Reference if this is a linked table
						RETURNS: Return an object who's first element is the generated string
										 and the second element is the key if a new table is required.
						EXAMPLE: (a . b) -> '(\"|a|b|\n\" '())"
	(let ((key (car elt))
				(val (cdr elt)))
		(cond ((not val) `(,(j2t-hp key "") nil))
					((vectorp val) `(,(j2t-hp key (j2t-lf key ref)) ,key))
					((consp val) `(,(j2t-hp key (j2t-lf key ref)) ,key))
					(t `(,(j2t-hp key (format "%s" val)) nil))
					)))

(defun j2t-tablify (elt &optional ref)
	"Function to be called recusrively to build an table.
						ELT: a json object
						REF: a reference is this is a linked table"
	(let ((cur "")
				(nex '()))
		(if j2t-debug (message (format "Got here! I was called with:\n  elt: %s\n  ref: %s\n" elt ref)))
		(if ref (j2t-c+ (format "#+name: %s\n" ref))) ;; If there's a reference add a name block to establish the linkage

		(cond
		 ;; ----- Element is a hash-map -----
		 ((consp elt)
			(progn
				(j2t-c+ "|key|value|\n|-\n") ;; Add headers for hashmap table
				;; For each element in the hashmap either add the value or add a link to the table of values
				(mapc (lambda (x) (let ((parsed (j2t-parse-hash-element x ref)))
														(format "x: %s\nparsed: %s" x parsed)
														(j2t-c+ (car parsed))
														(if (cadr parsed) (setq nex (append (cdr parsed) nex))))) elt)
				(j2t-c+ "\n")
				;; Recursively call this function to create any subtables
				(mapc (lambda (x)  (progn  (if j2t-debug (message (format "\nThe symbol I'm going to look up is: %s\n  it's type is: %s\n  and the value is: %s" x (type-of x) (alist-get x elt))))
																	 (if ref
																			 (j2t-c+ (j2t-tablify (alist-get x elt) (format "%s_%s" x ref)))
																		 (j2t-c+ (j2t-tablify (alist-get x elt) (format "%s" x)))))) nex)
				))

		 ;; ----- Element is a vector and is a vector of hash-maps -----
		 ((and (vectorp elt)
					 (consp (aref elt 0)))
			(let ((keys (mapc #'car (aref elt 0)))
						)
				(j2t-c+ (format "|%s|\n" (string-join "" (mapc (lambda (x) (format "%s" (car x))) keys))))
				(j2t-c+ "|-\n")
				(seq-map-indexed
				 (lambda (elt idx)
					 (let ((parsed (j2t-parse-hashmap-vector elt ref idx)))
						 (j2t-c+ "|")
						 (j2t-c+ (car parsed))
						 (j2t-c+ "\n")
						 (if (cadr parsed) (setq nex (append (cdr parsed) nex))))
					 ) elt)
				)

			;; Recursively call this function to create any subtables
			(mapc (lambda (x) (let ((key (nth 0 x))
															(value (nth 1 x))
															(i (nth 2 x)))
													(j2t-c+ (j2t-tablify value (format "%s_%s%s" key ref (format "%s" i)) )))) nex)
			)

		 ;; ----- Element is a vector of vectors -----
		 ((and (vectorp elt)
					 (vectorp (aref elt 0)))
			(let ((a nil))
				(mapc (lambda (x) (j2t-c+ (j2t-parse-vector-vector x))) elt)
				(j2t-c+ "\n")
				))

		 ;; ----- Element is an empty vector -----
		 ((and (vectorp elt)
					 (= (length elt) 0))
			(j2t-c+ "| |\n")
			)

		 ;; ----- Element is a vector of strings -----
		 ((vectorp elt)
			(j2t-c+ (format "|%s|\n|-\n" ref))
			(mapc (lambda (x) (j2t-c+ "|" (j2t-cs x) "|" "\n")) elt)
			)
		 )
		cur
		)
	)

(defun json-to-org-table-parse-json-string (str)
	"Read a json string, parse it, and return a tablified string.
						STR: json string"
	(j2t-tablify (json-read-from-string str)))

(defun json-to-org-table-parse-json (js)
	"Read an Emacs json object, parse it, and return a tablified string.
						The json should be in the format:
						 - lists -> vectors
						 - hashmaps -> alist cons
						 - null -> \"\"
						 - bool -> :json-true / :json-false
						JS: json object"
	(j2t-tablify js))

(provide 'json-to-org-table)

						;;; json-to-org-table.el ends here

(defun my/json-buffer-to-org-table ()
	(interactive)
	(let ((buffer-json (buffer-substring (point-min) (point-max)))
				(new-buffer (get-buffer-create "org-json-buffer"))
				(split-width-threshold nil))
		(with-current-buffer new-buffer
			(insert (json-to-org-table-parse-json-string buffer-json)))
		(switch-to-buffer-other-window new-buffer)
		(org-mode)
		(visual-fill-column-mode nil)))

(defun dw/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (auto-fill-mode 0)
  (visual-line-mode 1)
  (setq evil-auto-indent nil)
  ;; Make sure org-indent face is available
  (require 'org-indent)
  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-indent nil :inherit '(org-hide fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-table nil :font "DejaVu Sans Mono"))

(defun dw/org-agenda-setup ()
  (define-key org-agenda-mode-map (kbd "SPC") nil))

(use-package org
  :hook
  (org-mode . dw/org-mode-setup)
  (org-mode . org-update-all-dblocks)
  (org-agenda-mode . dw/org-agenda-setup)
  :config
  (setq org-ellipsis " ▾"
        org-hide-emphasis-markers t
        org-link-elisp-confirm-function nil
        org-src-preserve-indentation t ;; do not put two spaces on the left
        org-src-tab-acts-natively t
        org-startup-align-all-tables t))

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

;; Replace list hyphen with dot
(font-lock-add-keywords 'org-mode
                        '(("^ *\\([-]\\) "
                           (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))



;; Centering Text
(defun efs/org-mode-visual-fill ()
  (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook (org-mode . efs/org-mode-visual-fill))

;; Babel
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (python . t)
   (shell . t)))

(setq org-confirm-babel-evaluate nil)

;; This is needed as of Org 9.2
(require 'org-tempo)

(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("py" . "src python"))

(setq org-src-tab-acts-natively t)

(setq
 org-agenda-start-with-log-mode t
 org-log-done 'time
 org-log-into-drawer t
 org-agenda-show-future-repeats nil)

(defun org-summary-todo (n-done n-not-done)
  "Switch entry to DONE when all subentries are done, to TODO otherwise."
  (let (org-log-done org-log-states)   ; turn off logging
    (org-todo (if (= n-not-done 0) "DONE" "TODO"))))

(add-hook 'org-after-todo-statistics-hook #'org-summary-todo)


(defun my/org-todo-after-state-change ()
  (message org-state))

(add-hook 'org-after-todo-state-change-hook #'my/org-todo-after-state-change)
(setq org-agenda-clockreport-parameter-plist '(:stepskip0 t :link t :maxlevel 2 :fileskip0 t))
(setq org-agenda-skip-scheduled-if-done t)
(setq org-agenda-skip-deadline-if-done t)


(defun my/org-roam-refresh-agenda-list-agenda ()
  (interactive)
  (setq org-agenda-files (my/org-roam-list-notes-by-tag "agenda")))

(add-hook 'org-agenda-mode-hook
          #'my/org-roam-refresh-agenda-list-agenda)

;; The buffer you put this code in must have lexical-binding set to t!
;; See the final configuration at the end for more details.

(defun my/org-roam-filter-by-tag (tag-name)
  (lambda (node)
    (member tag-name (org-roam-node-tags node))))

(defun my/org-roam-list-notes-by-tag (tag-name)
  (mapcar #'org-roam-node-file
          (seq-filter
           (my/org-roam-filter-by-tag tag-name)
           (org-roam-node-list))))

(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory "~/roam-notes")
  (org-roam-completion-everywhere t)
  (org-roam-capture-templates
   '(("D" "default" plain
      "%?"
      :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}")
      :unnarrowed t)
     ("d" "Dailies" plain
      ( file "~/roam-notes/templates/dailies.org")
      :target (file+head "dailies/%<%Y_%m_%d>.org" "#+title: ${title}")
      :unnarrowed t)
     ("p" "Project" plain
      ( file "~/roam-notes/templates/project.org")
      :target (file+head "projects/${slug}.org" "#+title: ${title}")
      :unnarrowed t)))
  :bind (:map org-mode-map
              ("s-i" . completion-at-point))
  :config
  (org-roam-setup)
  (setq org-agenda-files (my/org-roam-list-notes-by-tag "agenda")))

(defun org-roam-node-insert-immediate (arg &rest args)
  (interactive "P")
  (let ((args (cons arg args))
        (org-roam-capture-templates (list (append (car org-roam-capture-templates)
                                                  '(:immediate-finish t)))))
    (apply #'org-roam-node-insert args)))

(use-package org-tree-slide
  :custom
  (org-image-actual-width nil))

(defun efs/lsp-mode-setup()
  (setq lsp-headerline-breadcrumb-segments '(path-up-to-project file symbols))
  (lsp-headerline-breadcrumb-mode))

(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :hook (lsp-mode . efs/lsp-mode-setup)
  :config
  (lsp-enable-which-key-integration))

(use-package lsp-ui
  :hook (lsp-mode . lsp-ui-mode))

(setq lsp-ui-doc-position 'bottom)
(setq lsp-ui-sideline-enable nil)
(setq lsp-ui-sideline-show-hover nil)

(use-package lsp-treemacs
  :after lsp)

(use-package lsp-ivy)

(use-package lsp-pyright
  :ensure t
	:hook (python-mode . (lambda ()
                         (require 'lsp-pyright)
                         (lsp-deferred)))  ; or lsp-deferred
	:config
		(setq lsp-pyright-type-checking-mode "off")
)

(use-package company
  :after lsp-mode
  :hook (prog-mode . company-mode)
  :bind (:map company-active-map
              ("<tab>" . company-complete-selection))
  (:map lsp-mode-map
        ("<tab>" . company-indent-or-complete-common))
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0))

(use-package company-box
  :hook (company-mode . company-box-mode))

(use-package with-venv
  :ensure t)

(use-package dap-mode
  :after lsp-mode
  :commands dap-debug
  :hook ((python-mode . dap-ui-mode)
				 (python-mode . dap-mode))
  :config
  (eval-when-compile
    (require 'cl))
  (require 'dap-python)
  (require 'dap-lldb)

  ;; Temporal fix
  (defun dap-python--pyenv-executable-find (command)
    (with-venv (executable-find "python")))
  )
(add-hook 'dap-running-session-mode
  (set-window-buffer nil (current-buffer)))

(use-package python-mode
  :ensure t
  :hook
  (python-mode . lsp-deferred)
	(python-mode . treemacs)
	(python-mode . lsp-treemacs-symbols)
  :custom
  (python-shell-interpreter "/usr/bin/python")
  (dap-python-executable "python")
  (dap-python-debugger 'debugpy)
	:config
	(electric-pair-mode t)
	(electric-indent-mode t)
 	(electric-indent-inhibit t)
	(indent-tabs-mode nil)
	(setq python-indent-offset 2))

(use-package pyvenv
  :config
  (pyvenv-mode 1)
)

(use-package poetry
  :ensure t)

(use-package web-mode
  :ensure t)

(add-to-list 'auto-mode-alist '("\\.jsx?$" . web-mode))
(setq web-mode-content-types-alist '(("jsx" . "\\.js[x]?\\'")))

(defun web-mode-init-hook ()
  "Hooks for Web mode.  Adjust indent."
  (setq web-mode-markup-indent-offset 2))

(add-hook 'web-mode-hook  'web-mode-init-hook)

(use-package flycheck
  :ensure t)

(setq-default flycheck-disabled-checkers
              (append flycheck-disabled-checkers
                      '(javascript-jshint json-jsonlist)))

;; Enable eslint checker for web-mode
(flycheck-add-mode 'javascript-eslint 'web-mode)

;; Enable flycheck globally
(add-hook 'after-init-hook #'global-flycheck-mode)

(use-package add-node-modules-path
  :ensure t)

(add-hook 'flycheck-mode-hook 'add-node-modules-path)

(use-package prettier-js
  :after js2-mode
	:hook
	(setq electric-pair-mode t)
	(setq electric-indent-mode t)
 	(setq electric-indent-inhibit t)
  :init
  (add-hook 'js2-mode-hook 'prettier-js-mode)
  (add-hook 'web-mode-hook 'prettier-js-mode)
  :config
  (setq prettier-js-args '("--trailing-comma" "all"
                           "--bracket-spacing" "false")))
(use-package emmet-mode
  :ensure t)

(add-hook 'web-mode-hook 'emmet-mode)

(use-package json-navigator
  :ensure t)

(use-package simple-httpd
  :ensure t)

(defun my/get-tangle-file-name (&optional buffer-name)
  (let (filename)
    (progn
      (save-current-buffer
        (set-buffer (or buffer-name (current-buffer)))
        (save-excursion
          (goto-char (point-min))
          (search-forward ":tangle" nil t)
          (forward-char)
          (setq filename (thing-at-point 'filename t)))))
    filename))

(defun my/get-stow-file-name (&optional buffer-name)
  (let (filename)
    (progn
      (save-current-buffer
        (set-buffer (or buffer-name (current-buffer)))
        (save-excursion
          (goto-char (point-min))
          (setq-local found (search-forward ":stow" nil t))
          (forward-char)
          (when found
            (setq filename (thing-at-point 'filename t))))))
    filename))

(defun my/stow-file (stow-file &optional from-tangle)
  ;; If symlink already exists, do nothing
  (unless (file-symlink-p stow-file)
    ;; Ensure directories exist
    (unless (file-exists-p (file-name-directory stow-file))
      (let ((answer (read-from-minibuffer (format "%s does not exist. Create it (y/N)?" (file-name-directory stow-file)))))
        (unless (or
                 (string-equal answer "N")
                 (string-equal answer "n")
                 (string-equal answer ""))
          (make-directory (file-name-directory stow-file) t))))
    ;; If file exists, move it to <filename>.bak
    (when (file-exists-p stow-file)
      (rename-file stow-file (concat stow-file ".bak")))
    ;; Create symbolic link
    (when from-tangle (setq-local tangle-name (my/get-tangle-file-name)))
    (make-symbolic-link (expand-file-name (or tangle-name buffer-file-name)) (expand-file-name stow-file))))

(defun my/handle-stow-header ()
  (when (s-ends-with-p buffer-file-name ".org")
    (let ((stow-file (my/get-stow-file-name))
          (tangle-file (my/get-tangle-file-name)))
      (when stow-file
        (my/stow-file stow-file (length tangle-file))))))

(add-hook 'after-save-hook 'my/handle-stow-header)

(add-hook 'after-save-hook
          (lambda ()
            (interactive)
            (when (string-match ".org" (buffer-file-name))
              (org-babel-tangle))))

(general-define-key
 :states 'normal
 :keymaps 'override
 :prefix "C-x"
 "C-x" 'save-buffers-kill-emacs)


(general-define-key
 :states 'normal
 :keymaps 'override
 :prefix "SPC"
 "g t" 'toggle-transparency)

(general-define-key
 :states 'normal
 :keymaps '(override org-agenda-keymap)
 :prefix "SPC"
 "w" '(nil :wk "Window")
 "w h" 'evil-window-left
 "w l" 'evil-window-right
 "w j" 'evil-window-down
 "w k" 'evil-window-up
 "w c" 'evil-window-delete
 "w w" 'evil-window-prev
 "w e" 'evil-window-next
 "w r" 'revert-buffer)

(evil-global-set-key 'normal
                     (kbd "C-l") 'evil-window-increase-width)
(evil-global-set-key 'normal
                     (kbd "C-h") 'evil-window-decrease-width)
(evil-global-set-key 'normal
                     (kbd "C-k") 'evil-window-increase-height)
(evil-global-set-key 'normal
                     (kbd "C-j") 'evil-window-decrease-height)

(general-define-key
 :states 'normal
 :keymaps 'override
 :prefix "SPC"
 "h" '(nil :wk "Help")
 "h f" 'counsel-describe-function
 "h v" 'counsel-describe-variable
 "h k" 'general-describe-keybindings
 "h m" 'describe-mode
 "h h" 'info-display-manual)

(general-define-key
 :states 'normal
 :keymaps 'override
 :prefix "SPC"
 "TAB" '(nil :wk "Workspaces")
 "TAB n" 'tabspaces-create-workspace
 "TAB c" 'tabspaces-close-workspace
 "TAB 1" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "1")) :which-key "Workspace: 1")
 "TAB 2" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "2")) :which-key "Workspace: 2")
 "TAB 3" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "3")) :which-key "Workspace: 3")
 "TAB 4" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "4")) :which-key "Workspace: 4")
 "TAB 5" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "5")) :which-key "Workspace: 5")
 "TAB 6" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "6")) :which-key "Workspace: 6")
 "TAB 7" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "7")) :which-key "Workspace: 7")
 "TAB 8" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "8")) :which-key "Workspace: 8")
 "TAB 9" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "9")) :which-key "Workspace: 9")
 "TAB 0" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "0")) :which-key "Workspace: 0"))

(general-define-key
 :states 'normal
 :keymaps 'override
 :prefix "SPC"
 "b" '(nil :wk "Buffer")
 "b b" 'buffer-menu
 "b c" '((lambda () (interactive) (kill-buffer (current-buffer))) :wk "Kill Buffer")
 "b k" 'kill-buffer-and-window
 "`" 'switch-to-prev-buffer)

(general-define-key
 :states '(normal)
 :keymaps 'override
 :prefix "SPC"
 "d" '(nil :wk "Execute/Eval")
 ":" 'counsel-M-x
 "d e b" 'eval-buffer
 "d e d" 'eval-defun
 "d e r" 'eval-region
 "d e e" 'eval-expression
 "d e l" 'eval-last-sexp)

(general-define-key
 :states '(normal visual) 
 :keymaps 'override
 :prefix "SPC"
 "f" '(nil :wk "Format")
 "f f" 'indent-region
 "f w" 'whitespace-mode)

(general-define-key
 :states 'insert
 :keymaps 'override
 "TAB" '(lambda () (interactive) (insert "\t"))
 "C-h" 'left-char
 "C-j" 'next-line
 "C-k" 'previous-line
 "C-l" 'right-char)

(evil-global-set-key 'normal (kbd "u") 'undo-tree-undo)
(evil-global-set-key 'normal (kbd "r") 'undo-tree-redo)

(general-define-key
 :states 'normal
 :keymaps 'override
 :prefix "SPC"
 "n" '(nil :wk "File Management")
 "." 'counsel-find-file
 "n h" '((lambda() (interactive) (dired-at-point "~/")) :which-key "File Browse: Home")
 "n p" '((lambda() (interactive) (dired-at-point "~/projects")) :which-key "File Browse: Projects")
 "n l" '((lambda() (interactive) (dired-at-point "~/linux-scripts")) :which-key "File Browse: Linux Scripts & Configs")
 "n c" '((lambda() (interactive) (dired-at-point "~/.config")) :which-key "File Browse: XDG Configs Directory")
 "n r" 'counsel-recentf
 "n n" 'dired-create-empty-file)

(evil-collection-define-key 'normal 'dired-mode-map
  "h" 'dired-up-directory
  "l" 'dired-find-file)

(general-define-key
 :states 'normal
 :keymaps 'override
 :prefix "SPC"
 "m" '(nil :wk "Magit")
 "m m" 'magit)

(general-define-key
 :states 'normal
 :keymaps 'override
 :prefix "SPC"
 "d" '(nil :wk "Run/Debug")
 "d o p" 'run-python
 "d e p" 'python-shell-send-buffer
 "d l f" 'flymake-mode)

(general-define-key
 :states 'normal
 :keymaps 'python-mode-map
 :prefix "SPC"
 "d" '(nil :wk "Run/Debug")
 "d v a" 'pyvenv-activate
 "d v w" 'pyvenv-workon)

(general-define-key
 :states 'normal
 :keymaps 'override
 :prefix "SPC"
 "d" '(nil :wk "Run/Debug")
 "d u t" 'lsp-ivy-workspace-symbol)

(general-define-key
 :states 'normal
 :keymaps '(lsp-mode-map python-mode-map)
 :prefix "SPC"
 "d" '(nil :wk "Run/Debug")
 "d d b" 'dap-breakpoint-toggle
 "d d d" 'dap-debug
 "d d l" 'dap-debug-last
 "d d w a" 'dap-ui-expressions-add
 "d d w d" 'dap-ui-expressions-remove
 "d d n" 'dap-next
 "d d r" 'dap-debug-restart
 "d d i" 'dap-step-in) 

(general-define-key
 :states 'normal
 :keymaps 'dap-mode-map
 "C-n" 'dap-next)

	(general-define-key
		:states '(normal visual)
		:map :override
		:prefix "SPC"
		"d c r" 'comment-or-uncomment-region
    "d f d" 'lsp-find-definition
    "d f r" 'lsp-find-references)

(general-define-key
 :states '(normal visual insert)
 :keymaps 'org-mode-map
 "C-h" 'org-do-promote
 "C-l" 'org-do-demote
 "C-S-h" 'org-promote-subtree
 "C-S-l" 'org-demote-subtree)

(general-define-key
 :states 'visual
 :keymaps 'org-mode-map
 :prefix "SPC"
 "o t" 'org-table-create-or-convert-from-region)

(general-define-key
 :states '(normal insert)
 :keymaps '(org-mode-map orgtbl-mode-map)
 :predicate '(org-at-table-p)
 "C-i r" 'org-table-insert-row
 "C-i c" 'org-table-insert-column
 "C-h" 'org-table-previous-field
 "C-l" 'org-table-next-field
 "C-j" 'org-table-next-row
 "C-k" 'evil-previous-line
 "C-r c" 'org-table-delete-column
 "C-r r" 'org-table-kill-row
 "C-S-k" 'org-table-move-row-up
 "C-S-j" 'org-table-move-row-down
 "C-S-h" 'org-table-move-column-left
 "C-S-l" 'org-table-move-column-right) 

(general-define-key
 :keymaps 'org-agenda-mode-map
 "j" 'org-agenda-next-line
 "k" 'org-agenda-previous-line)

(general-define-key
 :keymaps 'org-agenda-mode-map
 :prefix "SPC"
 "t a d" 'org-agenda-day-view)

(defun my/new-daily-note ()
  (interactive)
  (let ((org-roam-capture-templates
         '(("d" "Dailies" plain
            ( file "~/roam-notes/templates/dailies.org")
            :target (file+head "dailies/%<%Y_%m_%d>.org" "#+title: ${title}")
            :unnarrowed t))))
    (org-roam-node-find nil (format-time-string "Dailies - %Y-%m-%d" (current-time)) nil )))

(general-define-key
 :states 'normal
 :keymaps 'override
 :prefix "SPC"
 "r" '(nil :wk "Org Roam")
 "r l" 'org-roam-buffer-toggle
 "r f" 'org-roam-node-find
 "r i" 'org-roam-node-insert
 "r d" 'my/new-daily-note)

(general-define-key
 :states 'normal
 :keymaps 'override
 :prefix "SPC"
 "t" '(nil :wk "Org Agenda")
 "t t" 'org-todo
 "t a" 'org-agenda
 "t c" 'org-toggle-checkbox
 "t f a" 'my/org-roam-refresh-agenda-list-agenda)

(general-define-key
 :states 'normal
 :keymaps 'org-agenda-mode-map
 "t" 'org-agenda-todo
 "i" 'org-agenda-clock-in
 "o" 'org-agenda-clock-out
 "n" 'org-agenda-toggle-time-grid
 "l" 'org-agenda-log-mode)

																				;
(evil-set-initial-state 'org-agenda-mode 'normal)

(defun my/open-vterm-below ()
    (interactive)
    (split-window-below 35)
    (other-window 1)
    (vterm))

(general-define-key
 :states 'normal
 :keymaps 'override
 :prefix "SPC"
 "v t" 'my/open-vterm-below)

  (custom-set-variables
   ;; custom-set-variables was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   '(package-selected-packages
     '(forge evil-magit magit counsel-projectile projectile doom-themes ivy-rich rainbow-delimiters ivy org-babel org-mode general which-key use-package evil-collection)))
  (custom-set-faces
   ;; custom-set-faces was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   )
